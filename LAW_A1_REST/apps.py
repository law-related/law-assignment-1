from django.apps import AppConfig


class LawA1RestConfig(AppConfig):
    name = 'LAW_A1_REST'
