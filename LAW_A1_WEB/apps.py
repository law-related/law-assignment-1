from django.apps import AppConfig


class LawA1WebConfig(AppConfig):
    name = 'LAW_A1_WEB'
