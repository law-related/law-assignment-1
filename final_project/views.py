from django.shortcuts import render
from django.core.paginator import Paginator
import SPARQLWrapper
import pprint
from difflib import SequenceMatcher
from rdflib import Graph, URIRef
from rdflib.namespace import RDFS, SKOS
from .rdf_utils import dbpedia_query,query_local_gema


#tes
memoization = {}



def home(request):
    html = 'homepage.html'
    return render(request,html)

def string_is_similar(a,b):
    return SequenceMatcher(None, a, b).ratio()


def airline_detail(request,attribute):



    key = ["name", "country", "isActive", "callsign", "iata", "icao"]
    attributes = [(x.replace("%20"," ")).replace("_"," ") for x in  attribute.split('&')]

    response = dict(zip(key,attributes))
    dbpedia_query_res = dbpedia_query(response['name'])
    pp = pprint.PrettyPrinter(indent=1)
    pp.pprint(dbpedia_query_res)
    index_of_right_airline = 0
    highest_similarity_ratio = 0

    if(len(dbpedia_query_res) >0):


        for airline in dbpedia_query_res:
            current_score = string_is_similar(airline['AirlineName']['value'], response['name'])
            if ( current_score > highest_similarity_ratio):
                highest_similarity_ratio =current_score
                index_of_right_airline = dbpedia_query_res.index(airline)
        the_right_airline = dbpedia_query_res[index_of_right_airline]
        if ('abstract' in the_right_airline):

            response['abstract'] = the_right_airline['abstract']['value']
        else:
            response['abstract'] = " - "
        if ('comment' in the_right_airline):

            response['comment'] = the_right_airline['comment']['value']
        else:
            response['comment'] = " - "
        if ('FleetSize' in the_right_airline):

            response['fleet_size'] = the_right_airline['FleetSize']['value']
        else:
            response['fleet_size'] = " - "
        if ('Headquarter' in the_right_airline):

            response['headquarter_link'] = the_right_airline['Headquarter']['value']
            response['headquarter'] = response['headquarter_link'].split("/")[-1].replace('_', " ")
        else:
            response['headquarter_link'] = " - "
            response['headquarter_link'] = " - "
        if ('Slogan' in the_right_airline):

            response['slogan'] = the_right_airline['Slogan']['value']
        else:
            response['slogan'] = " - "

        if ('AirlineName' in the_right_airline):

            response['name'] = the_right_airline['AirlineName']['value'].replace('_'," ")
        if ('hubAirport' in the_right_airline):
            response['hub_airport_link'] = the_right_airline['hubAirport']['value']
            response['hub_airport'] = response['hub_airport_link'].split("/")[-1].replace('_', " ")
        else:
            response['hub_airport_link'] = " - "
            response['hub_airport'] = " - "





    print('the right airline:\n\n',the_right_airline)




    if (response['isActive'].lower() == 'yes'):
        response['status'] = 'Active'
    else:
        response['status'] = 'Deceased'
    print('attribute obtained:\n', response)
    html = 'infobox.html'
    return render(request,html,response)




def display_query_result(request):
    response = {}

    print('current method:\n',request.method)
    html = 'resultspage.html'
    if (request.method == 'POST'):
        query_results = query_local_gema(request.POST['airlineName'])
        paginator = Paginator(query_results, 7)
        memoization['paginator_object'] = paginator
        response['query_results'] =  memoization['paginator_object'].page(1)
        print('query_result:\n',response['query_results'])
        print('len query result:\n',len(response['query_results']))
    else:
        current_page = request.GET.get('page')
        print('current page:\n',current_page)
        response['query_results'] = memoization['paginator_object'].page(current_page)
    return  render(request,html,response)

